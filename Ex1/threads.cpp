#include "threads.h"
#include <Windows.h>
#include <time.h>
#include <math.h>
#include <mutex>

typedef std::thread Thread;
typedef std::mutex Mutex;

void I_Love_Threads()
{
	std::cout << "I love threads (but not in cpp)" << std::endl;
}

void call_I_Love_Threads()
{
	Thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	for (int i : primes)
		std::cout << i << std::endl;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i < end; i++) {
		bool prime = true;

		if (i < 2)
			prime = false;

		for (int j = 2; j <= i / 2 && prime; j++) {
			if (i % j == 0)
				prime = false;
		}

		if (prime)
			primes.push_back(i);
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> vec;

	const double sysTime = time(0);

	Thread thread(getPrimes, begin, end, std::ref(vec));
	thread.join();
	
	std::cout << "Took " << (time(0) - sysTime) << " seconds to calculate primes from " << begin << " to " << end << std::endl;
	
	return vec;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	vector<int> vec;
	static Mutex mtx;

	for (int i = begin; i < end; i++) {
		bool prime = true;

		if (i < 2)
			prime = false;

		for (int j = 2; j <= sqrt(i) && prime; j++) {
			if (i % j == 0)
				prime = false;
		}

		mtx.lock();

		if (prime) {
			file << i;
			file << std::endl;
		}

		mtx.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int sections = (end - begin) / N;
	

	ofstream file;
	file.open(filePath);
	vector<Thread> threads;

	const double sysTime = time(0);

	for (int i = 0; i < N; i++) {
		threads.push_back(Thread(writePrimesToFile, begin + (sections * i), begin + (sections * (i + 1)), std::ref(file)));
	}

	for (int i = 0; i < N; i++) {
		threads[i].join();
	}

	file.close();

	std::cout << "Took " << (time(0) - sysTime) << " seconds to write all primes from " << begin << " to " << end << std::endl;
}
