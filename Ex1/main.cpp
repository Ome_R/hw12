#include "threads.h"

int main()
{
	////0-1000
	//vector<int> vec1 = callGetPrimes(0, 1000);
	////0-100000
	//vector<int> vec2 = callGetPrimes(0, 100000);
	////0-1000000
	//vector<int> vec3 = callGetPrimes(0, 1000000);

	//0-1000
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	//0-100000
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 4);
	//0-1000000
	//callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 2);

	system("pause");
	return 0;
}