#include "MessagesSender.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <set>

#define SIGN_IN_ACTION 1
#define SIGN_OUT_ACTION 2
#define SHOW_USERS_ACTION 3
#define EXIT_ACTION 4

using std::cout;
using std::endl;
using std::cin;

typedef std::thread Thread;

void readMessages(MessagesSender & sender);
void writeMessages(MessagesSender & sender);
void addQueue(Queue src, Queue toAdd);

bool writeFlag = false;

int main() {
	MessagesSender sender;

	Thread(readMessages, std::ref(sender)).detach();
	Thread(writeMessages, std::ref(sender)).detach();

	int action;

	do {
		action = sender.showMenu();

		if (action != EXIT_ACTION)
			system("cls");

		switch (action)
		{
			case SIGN_IN_ACTION:
				sender.signinUser();
				break;
			case SIGN_OUT_ACTION:
				sender.signoutUser();
				break;
			case SHOW_USERS_ACTION:
				sender.showConnectedUsers();
				break;
			case EXIT_ACTION:
				std::cout << "Good bye!" << std::endl;
				break;
		}

		if (action != EXIT_ACTION) {
			std::this_thread::sleep_for(std::chrono::seconds(3));
			system("cls");
		}

	} while (action != EXIT_ACTION);

	system("pause");

	return 0;
}

void readMessages(MessagesSender & sender) {
	sender.readMessages();
}

void writeMessages(MessagesSender & sender) {
	sender.writeMessages();
}