#include "MessagesSender.h"
#include <iostream>
#include <string>
#include <fstream>
#include <set>
#include <thread>
#include <condition_variable>
#include <Windows.h>

using std::cout;
using std::endl;
using std::cin;
using std::fstream;
using std::ofstream;

#define READ_FILE_PATH "data.txt"
#define WRITE_FILE_PATH "output.txt"
#define Sleep std::this_thread::sleep_for
#define Minutes std::chrono::minutes

void addQueue(Queue & src, Queue & toAdd);
void clearFile(const char* path);

int MessagesSender::writeFlag = false;

int MessagesSender::showMenu()
{
	cout << "1. Signin" << endl << "2. Signout" << endl << "3. Connected Users" << endl << "4. Exit" << endl << "What is your choise? ";
	int action = 4;
	cin >> action;
	return action;
}

void MessagesSender::signinUser()
{
	string name = "";
	cout << "Enter a name: ";
	cin >> name;

	Set::iterator iter = users.find(name);

	if (iter == users.end()) {
		users.insert(name);
		cout << "Added " << name << " into the system." << endl;
	}
	else {
		cout << "This user is already exists in the system." << endl;
	}
}

void MessagesSender::signoutUser()
{
	string name = "";
	cout << "Enter a name: ";
	cin >> name;

	Set::iterator iter = users.find(name);

	if (iter == users.end()) {
		cout << "This user is does not exist in the system." << endl;
	}
	else {
		users.erase(name);
		cout << "Removed " << name << " from the system." << endl;
	}
}

void MessagesSender::showConnectedUsers()
{
	cout << "Connected Users: ";

	bool space = false;
	for (Set::iterator it = users.begin(); it != users.end(); it++) {
		if (space) {
			cout << ", ";
		}
		else {
			space = true;
		}
		cout << *it;
	}

	cout << endl;
}

void MessagesSender::readMessages()
{
	while (true) {
		Queue messages;

		fstream file;
		file.open(READ_FILE_PATH);
		string line;

		if (file.is_open()) {
			while (std::getline(file, line)) {
				messages.push(line);
			}
			
			file.close();
			clearFile(READ_FILE_PATH);

			mtx.lock();
			addQueue(this->messages, messages);
			mtx.unlock();

			writeFlag = true;
			cv.notify_all();
		}

		else {
			cout << "Couldn't open file " << READ_FILE_PATH << "." << endl;
		}

		Sleep(Minutes(1));
	}
}

void MessagesSender::writeMessages()
{
	while (true) {
		writeFlag = false;

		mtx.lock();
		Queue messages = this->messages;
		Queue empty;
		std::swap(empty, this->messages);
		mtx.unlock();

		ofstream file;
		file.open(WRITE_FILE_PATH);
		string message;

		while (messages.size() > 0) {
			message = messages.front();
			for (string user : users) {
				file << user << ": " << message << endl;
			}
			messages.pop();
		}

		file.close();
		UniqueLock lock(mtx);
		cv.wait(lock, [] {return writeFlag; });
	}
}

void addQueue(Queue & src, Queue & toAdd)
{
	while (toAdd.size() > 0) {
		src.push(toAdd.front());
		toAdd.pop();
	}
}

void clearFile(const char* path) {
	fstream file;
	file.open(path, std::ios::out | std::ios::trunc);
	file.clear();
	file.close();
}
