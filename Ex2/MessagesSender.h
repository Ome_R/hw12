#pragma once

#include <iostream>
#include <set>
#include <queue>
#include <mutex>

using std::string;

typedef std::set<string> Set;
typedef std::queue<string> Queue;
typedef std::mutex Mutex;
typedef std::unique_lock<std::mutex> UniqueLock;
typedef std::condition_variable ConditionVariable;

class MessagesSender {
private:
	Set users;
	Queue messages;
	Mutex mtx;

	ConditionVariable cv;

	static int writeFlag;

public:
	int showMenu();
	void signinUser();
	void signoutUser();
	void showConnectedUsers();

	void readMessages();
	void writeMessages();

};